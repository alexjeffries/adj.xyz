---
title: About
---

Projects
--------

Projects and open source contributions can be found at

- [GitLab](https://gitlab.com/alexjeffries)
- [GitHub](https://github.com/alexjeffries)

Employment
----------

Staff Software Engineer at The Home Depot

Education
---------

B.S., Applied Mathematics, Georgia Institute of Technology, 2012

GPG Key
-------

[Here](/static/alexjeffries-gpg.txt)
